﻿using BagFeeApi.Models;
using System;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace BagFeeApi.ApplicationLogic
{
    public class BagFeeAppLogic : IBagFeeAppLogic
    {
        private readonly string _bagFeePath = $"{AppContext.BaseDirectory}\\DataFiles\\BagFees.xml";

        public string GetBagFee(BaggageModel model)
        {

            var originMkt = GetMarket(model.Origin);
            var destMkt = GetMarket(model.Dest);

            var bagFees = ReadBagFeeData();

            if (bagFees?.bagFee?.Length > 0)
            {
                var defaultBagFee = bagFees.bagFee.FirstOrDefault(x => x.type == "default");

                if (defaultBagFee != null)
                {
                    var route = defaultBagFee.route.FirstOrDefault(x => x.originRoute == originMkt.ToString() && x.destRoute == originMkt.ToString());

                    if (route != null)
                    {
                        var bagoutputString = "";
                        if (originMkt == 3 || destMkt == 3)
                        {
                            var routeIntl = route.TravelType.FirstOrDefault(x => x.type == "pnrHasInternationalFlight");
                            bagoutputString = routeIntl.fee.Select(x=>$"{x.fee}Bag =\\n{GetBagFeeBuilder(x.v)}").Aggregate((current, next) => current + "\\n" + next);                           
                        }

                        else if(model.Class==ClassType.First)
                        {
                            var routeIntl = route.TravelType.FirstOrDefault(x => x.type == "journeyHasFirstClassFlight");
                            bagoutputString = routeIntl.fee.Select(x => $"{x.fee} Bag =\\n{GetBagFeeBuilder(x.v)}").Aggregate((current, next) => current + "\\n" + next);
                        }
                        else
                        {
                            var routeIntl = route.TravelType.FirstOrDefault(x => x.type == "default");
                            bagoutputString = routeIntl.fee.Where (x=>x.fee== "normal"|| x.fee== "overweight").Select(x => $"{x.fee} Bag =\\n{GetBagFeeBuilder(x.v)}").Aggregate((current, next) => current + "\\n" + next);
                        }


                        return bagoutputString;
                    }
                }
            }

            return string.Empty;
        }

        private bagFees ReadBagFeeData()
        {
            bagFees bags = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(bagFees));

                // A FileStream is needed to read the XML document.
                FileStream fs = new FileStream(_bagFeePath, FileMode.Open);



                using (XmlReader reader = XmlReader.Create(fs))
                {

                    // Declare an object variable of the type to be deserialized.
                    bags = (bagFees)serializer.Deserialize(reader);
                }
                fs.Close();
            }
            catch (IOException io)
            {
                bags = null;
            }

            return bags;
        }

        private int GetMarket(string CityName)
        {
            HACityList haCity = new HACityList();
            haCity.ReadCities();
            return haCity.GetMarketOftheCity(CityName);
        }

        private string GetBagFeeBuilder(string priceString)
        {
            var priceList = priceString.Split(',').ToList();

            string priceBuilder = string.Empty;
            if (priceList?.Count > 0)
            {
                int count = 1;
                priceList = priceList.Count > 1 ? priceList.Take(2).ToList() : priceList;
                foreach (var price in priceList)
                {
                    var priceStr = price == "0" ? "Free" : $"${price}";
                    priceBuilder = priceBuilder + $"{AddOrdinal(count)} bag : { priceStr} \\n";
                    count++;
                }

            }

            return priceBuilder;
        }

        private string AddOrdinal(int num)
        {
            if (num <= 0) return num.ToString();
            switch (num % 100)
            {
                case 11:
                case 12:
                case 13:
                    return num + "th";
            }
            switch (num % 10)
            {
                case 1:
                    return num + "st";
                case 2:
                    return num + "nd";
                case 3:
                    return num + "rd";
                default:
                    return num + "th";
            }
        }
    }
}

﻿using BagFeeApi.Models;

namespace BagFeeApi.ApplicationLogic
{
    public interface IBagFeeAppLogic
    {
        string GetBagFee(BaggageModel model);
    }
}
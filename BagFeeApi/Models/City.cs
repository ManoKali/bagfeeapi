﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace BagFeeApi.Models
{

    public class HACityList
    {
        private CityList _cityList;
        private string _defaultPath = $"{AppContext.BaseDirectory }\\DataFiles\\cities.xml";
        
        public HACityList()
        {
            _cityList = new CityList();
        }

        public void ReadCities(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                path = _defaultPath;
            }
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(CityList));
                FileStream fs = new FileStream(path, FileMode.Open);
                XmlReader reader = XmlReader.Create(fs); 
                _cityList = (CityList)serializer.Deserialize(reader);
                reader.Dispose();
            }
            catch (Exception)
            {

            }
        }

        public City GetCity(string code)
        {
            return _cityList.Cities.City.Where(x => x.Code.ToUpper().Equals(code)).FirstOrDefault();
        }

        public List<City> GetInterIslandCities()
        {
            return _cityList.Cities.City.Where(x=>x.Market.Equals(1)).ToList();
        }

        public List<City> GetAllCities()
        {
            return _cityList.Cities.City.ToList();
        }

        public string GetCityName(string code)
        {
            return _cityList.Cities.City.Where(x => x.Code.ToUpper().Equals(code)).FirstOrDefault().DisplayName;
        }

        public int GetMarketOftheCity(string code)
        {
            return _cityList.Cities.City.Where(x => code.ToUpper().IndexOf(x.Code.ToUpper()) != -1).FirstOrDefault().Market;
        }

        public CityList ReadCities()
        {

            if (_cityList.Cities != null && _cityList.Cities.City != null && _cityList.Cities.City.Count > 0)
                return _cityList;

            int binIndex = _defaultPath.IndexOf("bin");

            string cityListPath = _defaultPath.Substring(0, _defaultPath.Length - (_defaultPath.Length - binIndex)) + "DataFiles\\Cities.xml";

            XmlSerializer serializer = new XmlSerializer(typeof(CityList));
            FileStream fs = new FileStream(cityListPath, FileMode.Open);
            using (XmlReader reader = XmlReader.Create(fs))
            {
                _cityList = (CityList)serializer.Deserialize(reader);
            }
            fs.Close();
            return _cityList;
        }
    }

    public static class FileStreamExtension
    {
        public static void Close(this FileStream reader)
        {
            reader.Dispose();
        }
    }

    public class Cities
    {
        [XmlElement("City")]
        public List<City> City = new List<City>();

        public Cities()
        {

        }
    }

    public class CityList
    {
        public Cities Cities { get; set; }

        public CityList()
        {
        }
    }

    
    public class City
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public string DisplayName { get; set; }

        public string Region { get; set; }

        public int Active { get; set; }

        public int Market { get; set; }

        public City()
        { }
    }
}

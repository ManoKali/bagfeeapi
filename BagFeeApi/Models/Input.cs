﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagFeeApi.Models
{
    public class BaggageModel
    {
        public string Origin { get; set; }
        public string Dest { get; set; }
        public ClassType Class { get; set; }
    }

    public enum ClassType
    {
        Economy = 0,
        First = 1,
        Business = 2
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagFeeApi.Models
{

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class bagFees
    {

        private bagFeesBagFee[] bagFeeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("bagFee")]
        public bagFeesBagFee[] bagFee
        {
            get
            {
                return this.bagFeeField;
            }
            set
            {
                this.bagFeeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bagFeesBagFee
    {

        private bagFeesBagFeeRoute[] routeField;

        private string typeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("route")]
        public bagFeesBagFeeRoute[] route
        {
            get
            {
                return this.routeField;
            }
            set
            {
                this.routeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bagFeesBagFeeRoute
    {

        private bagFeesBagFeeRouteFeeStartingWindow feeStartingWindowField;

        private bagFeesBagFeeRouteFeeEndingWindow feeEndingWindowField;

        private bagFeesBagFeeRouteMember[] membersField;

        private bagFeesBagFeeRouteTravelType[] travelTypeField;

        private string originRouteField;

        private string destRouteField;

        /// <remarks/>
        public bagFeesBagFeeRouteFeeStartingWindow FeeStartingWindow
        {
            get
            {
                return this.feeStartingWindowField;
            }
            set
            {
                this.feeStartingWindowField = value;
            }
        }

        /// <remarks/>
        public bagFeesBagFeeRouteFeeEndingWindow FeeEndingWindow
        {
            get
            {
                return this.feeEndingWindowField;
            }
            set
            {
                this.feeEndingWindowField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Member", IsNullable = false)]
        public bagFeesBagFeeRouteMember[] Members
        {
            get
            {
                return this.membersField;
            }
            set
            {
                this.membersField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("TravelType")]
        public bagFeesBagFeeRouteTravelType[] TravelType
        {
            get
            {
                return this.travelTypeField;
            }
            set
            {
                this.travelTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string originRoute
        {
            get
            {
                return this.originRouteField;
            }
            set
            {
                this.originRouteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string destRoute
        {
            get
            {
                return this.destRouteField;
            }
            set
            {
                this.destRouteField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bagFeesBagFeeRouteFeeStartingWindow
    {

        private bagFeesBagFeeRouteFeeStartingWindowFee feeField;

        private uint dateField;

        /// <remarks/>
        public bagFeesBagFeeRouteFeeStartingWindowFee fee
        {
            get
            {
                return this.feeField;
            }
            set
            {
                this.feeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public uint date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bagFeesBagFeeRouteFeeStartingWindowFee
    {

        private string feeField;

        private string vField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string fee
        {
            get
            {
                return this.feeField;
            }
            set
            {
                this.feeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string v
        {
            get
            {
                return this.vField;
            }
            set
            {
                this.vField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bagFeesBagFeeRouteFeeEndingWindow
    {

        private bagFeesBagFeeRouteFeeEndingWindowFee feeField;

        private uint dateField;

        /// <remarks/>
        public bagFeesBagFeeRouteFeeEndingWindowFee fee
        {
            get
            {
                return this.feeField;
            }
            set
            {
                this.feeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public uint date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bagFeesBagFeeRouteFeeEndingWindowFee
    {

        private string feeField;

        private string vField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string fee
        {
            get
            {
                return this.feeField;
            }
            set
            {
                this.feeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string v
        {
            get
            {
                return this.vField;
            }
            set
            {
                this.vField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bagFeesBagFeeRouteMember
    {

        private bagFeesBagFeeRouteMemberFeeStartWindow feeStartWindowField;

        private bagFeesBagFeeRouteMemberFeeEndWindow feeEndWindowField;

        private bagFeesBagFeeRouteMemberFee[] feeField;

        private string typeField;

        /// <remarks/>
        public bagFeesBagFeeRouteMemberFeeStartWindow FeeStartWindow
        {
            get
            {
                return this.feeStartWindowField;
            }
            set
            {
                this.feeStartWindowField = value;
            }
        }

        /// <remarks/>
        public bagFeesBagFeeRouteMemberFeeEndWindow FeeEndWindow
        {
            get
            {
                return this.feeEndWindowField;
            }
            set
            {
                this.feeEndWindowField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("fee")]
        public bagFeesBagFeeRouteMemberFee[] fee
        {
            get
            {
                return this.feeField;
            }
            set
            {
                this.feeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bagFeesBagFeeRouteMemberFeeStartWindow
    {

        private bagFeesBagFeeRouteMemberFeeStartWindowFee feeField;

        private uint dateField;

        /// <remarks/>
        public bagFeesBagFeeRouteMemberFeeStartWindowFee fee
        {
            get
            {
                return this.feeField;
            }
            set
            {
                this.feeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public uint date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bagFeesBagFeeRouteMemberFeeStartWindowFee
    {

        private string feeField;

        private string vField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string fee
        {
            get
            {
                return this.feeField;
            }
            set
            {
                this.feeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string v
        {
            get
            {
                return this.vField;
            }
            set
            {
                this.vField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bagFeesBagFeeRouteMemberFeeEndWindow
    {

        private bagFeesBagFeeRouteMemberFeeEndWindowFee feeField;

        private uint dateField;

        /// <remarks/>
        public bagFeesBagFeeRouteMemberFeeEndWindowFee fee
        {
            get
            {
                return this.feeField;
            }
            set
            {
                this.feeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public uint date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bagFeesBagFeeRouteMemberFeeEndWindowFee
    {

        private string feeField;

        private string vField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string fee
        {
            get
            {
                return this.feeField;
            }
            set
            {
                this.feeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string v
        {
            get
            {
                return this.vField;
            }
            set
            {
                this.vField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bagFeesBagFeeRouteMemberFee
    {

        private string feeField;

        private string vField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string fee
        {
            get
            {
                return this.feeField;
            }
            set
            {
                this.feeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string v
        {
            get
            {
                return this.vField;
            }
            set
            {
                this.vField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bagFeesBagFeeRouteTravelType
    {

        private bagFeesBagFeeRouteTravelTypeFee[] feeField;

        private bagFeesBagFeeRouteTravelTypeMode[] modeField;

        private string typeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("fee")]
        public bagFeesBagFeeRouteTravelTypeFee[] fee
        {
            get
            {
                return this.feeField;
            }
            set
            {
                this.feeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Mode")]
        public bagFeesBagFeeRouteTravelTypeMode[] Mode
        {
            get
            {
                return this.modeField;
            }
            set
            {
                this.modeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bagFeesBagFeeRouteTravelTypeFee
    {

        private string feeField;

        private string vField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string fee
        {
            get
            {
                return this.feeField;
            }
            set
            {
                this.feeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string v
        {
            get
            {
                return this.vField;
            }
            set
            {
                this.vField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bagFeesBagFeeRouteTravelTypeMode
    {

        private bagFeesBagFeeRouteTravelTypeModeFee[] feeField;

        private string typeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("fee")]
        public bagFeesBagFeeRouteTravelTypeModeFee[] fee
        {
            get
            {
                return this.feeField;
            }
            set
            {
                this.feeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bagFeesBagFeeRouteTravelTypeModeFee
    {

        private string feeField;

        private string vField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string fee
        {
            get
            {
                return this.feeField;
            }
            set
            {
                this.feeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string v
        {
            get
            {
                return this.vField;
            }
            set
            {
                this.vField = value;
            }
        }
    }


}

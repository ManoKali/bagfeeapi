using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BagFeeApi.ApplicationLogic;
using BagFeeApi.Models;

namespace BagFeeApi.Controllers
{
     
    [Route("api/Baggage")]
    public class BaggageController : Controller
    { 
        private readonly IBagFeeAppLogic _iBagFeeAppLogic;

        public BaggageController(IBagFeeAppLogic iBagFeeAppLogic)
        {
            _iBagFeeAppLogic = iBagFeeAppLogic;
        } 
        // POST: api/Baggage
        [HttpGet] 
        public string Get([FromBody] BaggageModel model)
        {
            var result = _iBagFeeAppLogic.GetBagFee(model);
            return result;
        }
        
      
    }
}
